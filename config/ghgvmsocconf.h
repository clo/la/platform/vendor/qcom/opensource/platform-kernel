/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (c) 2024, Qualcomm Innovation Center, Inc. All rights reserved.
 */

#define CONFIG_QCOM_SOCINFO_DT 1
#define CONFIG_VIRTIO_FASTRPC 1
#define CONFIG_HYBRID_FASTRPC 1
#define CONFIG_MSM_BOOT_MARKER 1
